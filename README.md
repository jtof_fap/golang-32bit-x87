# Golang 32bit-x87 Docker image #
A container to run Golang on old architecture.

- Node version: `1.6.3`

### Purpose ###

I have created this docker image in order to build and run the latest `Golang` version on an old 32bit server with athlon CPU (`i386/x87`: without SSE/SSE2 instructions) ...Fuck Yeah !!!...

Indeed, all other 32bit Golang image doesn't work because they use the pre-builded [official Golang binaries](https://golang.org/dl/) compiled with standards x86 SSE/SSE2 instructions. When i run them => 'Illegal instruction' => Game over.

The Official compiled `Golang` binaries are used in many other images: docker, gitlab, etc.. which all failed in my 32bit-x87 architecture...

### Official image 32 bits tweaks ###

This image is derivated from the official `golang 1.6.3` image with some tweaks and with the last source code instead of use the pre-builded "Golang" binaries:

- [https://hub.docker.com/_/golang/](https://hub.docker.com/_/golang/)
- [https://github.com/docker-library/golang/blob/master/1.6/wheezy/Dockerfile](https://github.com/docker-library/golang/blob/master/1.6/wheezy/Dockerfile)

To build this `go1.6.3-x87` version, I downloaded and compiled the `go1.4.3` version, then `go1.6.3` version with the following environment options:

    GOARCH=386
    GOOS=linux
    GO386=387
    GOARM=5

### Rebuild your own golang-x.x.x-x87 ###

Docker hub autobuild fail to compile golang 32bit on x64 architecure. This image just download and install my precompiled version of go1.6.3-x87. You can download or use in your images this version:

- [https://bitbucket.org/jtof_fap/golang-32bit-x87/raw/master/go1.6.3-x87.tgz](https://bitbucket.org/jtof_fap/golang-32bit-x87/raw/master/go1.6.3-x87.tgz)

If you want to build your own version, use the following Dockerfile on a full 32bit machine:  

- [https://bitbucket.org/jtof_fap/golang-32bit-x87/src/master/build/Dockerfile?fileviewer=file-view-default](https://bitbucket.org/jtof_fap/golang-32bit-x87/src/master/build/Dockerfile?fileviewer=file-view-default)


    git clone git@bitbucket.org:jtof_fap/golang-32bit-x87.git
    cd golang-32bit-x87/build
    [modify $GOLANG_VERSION]
    docker build -t "golang-x87:x.x.x" .
    [share...]

### Getting this image ###

    docker pull insecurity/golang-32bit-x87:latest
    docker pull insecurity/golang-32bit-x87:1.6.3

### Rebuild this image ###

    git clone git@bitbucket.org:jtof_fap/golang-32bit-x87.git
    cd golang-32bit-x87
    docker build -t "golang-32bit-x87:latest" .

### Usage and documentation ###

Reports to the official `Golang` repository:

- [https://hub.docker.com/_/golang/](https://hub.docker.com/_/golang/)

